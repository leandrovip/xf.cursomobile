﻿using XF.CursoMobile.Models;

namespace XF.CursoMobile.ViewModels
{
    public class CidadeConsultaViewModel : BaseViewModel
    {
        #region Propriedades

        #endregion

        #region Construtores

        #endregion

        #region Métodos Públicos

        #endregion

        #region Métodos Privados

        #endregion

        #region Constantes

        #endregion

        #region Métodos Estáticos

        #endregion

        public CidadeConsultaViewModel(Cidade cidade)
        {
            Cidade = cidade;
        }

        public Cidade Cidade { get; set; }
    }
}