﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.CursoMobile.Annotations;
using PropertyChangingEventArgs = Xamarin.Forms.PropertyChangingEventArgs;
using PropertyChangingEventHandler = Xamarin.Forms.PropertyChangingEventHandler;

namespace XF.CursoMobile.ViewModels
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        #region Propriedades

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanging([CallerMemberName] string propertyName = null)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }

        #endregion

        #region Modasl e Navigation

        // exibir uma tela modal
        public async Task PushModalAsync(Page page)
        {
            if (App.Navigation != null)
                await App.Navigation.PushModalAsync(page);
        }

        // Tirar um modal da navegação
        public async Task PopModalAsync()
        {
            if (App.Navigation != null)
                await App.Navigation.PopModalAsync();
        }

        // Chamar uma página/view
        public async Task PushAsync(Page page)
        {
            if (App.Navigation.IsNotNull())
                await App.Navigation.PushAsync(page);
        }

        // Remove uma uma página/view
        public async Task PopAsync()
        {
            if (App.Navigation.IsNotNull())
                await App.Navigation.PopAsync();
        }

        #endregion
    }
}