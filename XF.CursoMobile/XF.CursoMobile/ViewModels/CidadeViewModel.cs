﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Refit;
using Xamarin.Forms;
using XF.CursoMobile.Core;
using XF.CursoMobile.Models;
using XF.CursoMobile.Models.Interfaces;
using XF.CursoMobile.Views;

namespace XF.CursoMobile.ViewModels
{
    public class CidadeViewModel : BaseViewModel
    {
        private IEnumerable<Cidade> _cidades;
        private ICommand _goToCidadeConsultaPage;
        private Cidade _cidade;
        private bool _isBusy;
        private ICommand _atualizarCidades;

        public Cidade Cidade
        {
            get => _cidade;
            set
            {
                _cidade = value;

                if (_cidade.IsNull()) return;

                GoToCidadeConsultaPage.Execute(_cidade);
                OnPropertyChanged();

                _cidade = null;
            }
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        public ICommand GoToCidadeConsultaPage => _goToCidadeConsultaPage ?? (_goToCidadeConsultaPage = new Command<Cidade>(async cidade =>
        {
            var page = new CidadeConsultaPage(cidade);
            await PushAsync(page);
        }));

        public ICommand AtualizarCidades => _atualizarCidades ?? (_atualizarCidades = new Command(ObterLista));

        public IEnumerable<Cidade> Cidades
        {
            get => _cidades;
            set
            {
                _cidades = value;
                if (_cidades.IsNull()) return;
                OnPropertyChanged();
            }
        }

        public async void ObterLista()
        {
            try
            {
                IsBusy = true;
                var cidadeApi = RestService.For<ICidadeApi>(Endpoints.BaseUrl);
                var retorno = await cidadeApi.ObterLista();
                Cidades = retorno.Sucess ? retorno.Data : new List<Cidade>();
            }
            catch (Exception ex)
            {
                Cidades = new List<Cidade>();
                Console.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public CidadeViewModel()
        {
            ObterLista();
        }
    }
}