﻿namespace XF.CursoMobile.Core
{
    public class Result<T>
    {
        public bool Sucess { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}