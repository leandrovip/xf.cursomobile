﻿namespace XF.CursoMobile.Models
{
    public class Cidade
    {
        public int CidadeId { get; set; }
        public string Nome { get; set; }
        public int EstadoId { get; set; }
        public string Estado { get; set; }
    }
}