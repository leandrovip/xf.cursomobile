﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;
using XF.CursoMobile.Core;

namespace XF.CursoMobile.Models.Interfaces
{
    public interface ICidadeApi
    {
        [Get("/cidade")]
        Task<Result<IEnumerable<Cidade>>> ObterLista();

        [Get("/cidade/{cidadeId}")]
        Task<Result<Cidade>> Obter(int cidadeId);
    }
}