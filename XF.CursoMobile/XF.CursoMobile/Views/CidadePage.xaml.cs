﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.CursoMobile.ViewModels;

namespace XF.CursoMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CidadePage : ContentPage
    {
        public CidadePage()
        {
            InitializeComponent();
            BindingContext = new CidadeViewModel();
        }
    }
}