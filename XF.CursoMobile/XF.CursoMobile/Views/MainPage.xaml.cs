﻿using System;
using Xamarin.Forms;

namespace XF.CursoMobile.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void TxtEntrar_OnClicked(object sender, EventArgs e)
        {
            App.Navigation.PushAsync(new CidadePage());
        }
    }
}