﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.CursoMobile.Models;
using XF.CursoMobile.ViewModels;

namespace XF.CursoMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CidadeConsultaPage : ContentPage
    {
        public CidadeConsultaPage(Cidade cidade)
        {
            InitializeComponent();
            BindingContext = new CidadeConsultaViewModel(cidade);
        }
    }
}